# amberframework-amber 

[amberframework/amber](https://github.com/amberframework/amber) web application framework written in Crystal inspired by Kemal, Rails, Phoenix and other. [amberframework.org](https://amberframework.org)